require 'active_record'
require 'activerecord-import'
require 'pry'
require 'json'
require 'parallel'

def random_int(range=10)
  rand(range)
end

def random_asset_id
  random_int
end

def random_package_name
  "ios_app_#{random_int}"
end

def import_keys
  [:asset_id, :package_name, :foo]
end

def duplicate_keys
  [:foo]
end

def params
  [
    [random_asset_id, random_package_name, random_int],
    [random_asset_id, random_package_name, random_int],
    [random_asset_id, random_package_name, random_int],
    [random_asset_id, random_package_name, random_int],
    [random_asset_id, random_package_name, random_int],
    [random_asset_id, random_package_name, random_int],
  ]
end

EXEC_COUNT = 1000
PARALLEL_COUNT = 4

conf = YAML.load_file('./database.yml')
ActiveRecord::Base.establish_connection(conf['development'])

# Timezoneの設定
Time.zone_default = Time.find_zone! 'Tokyo'
ActiveRecord::Base.default_timezone = :local

# ロガーの設定
ActiveRecord::Base.logger = Logger.new(STDOUT)

# Modelを定義
class IodkuTable < ActiveRecord::Base
end

Parallel.each(1..EXEC_COUNT, in_processes: PARALLEL_COUNT) do
  IodkuTable.import import_keys, params, on_duplicate_key_update: duplicate_keys
end
