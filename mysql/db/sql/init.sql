USE iodku_database;

DROP TABLE IF EXISTS iodku_table;

CREATE TABLE `iodku_tables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asset_id` bigint(20) NOT NULL,
  `package_name` CHAR(30) NOT NULL,
  `foo` int(11) NOT NULL,

  PRIMARY KEY (id),
  UNIQUE id_package_name (asset_id, package_name)
)
